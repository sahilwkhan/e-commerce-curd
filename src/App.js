import { Routes, Route } from 'react-router-dom';
import ProductPage from './components/ProductPage/ProductPage';
import { useEffect, useState } from 'react';
import axios from 'axios';

import Header from './components/layout/Header';
import Loader from './components/Loader/Loader';
import Add from './components/CURD/Add/Add';
import Home from './components/Home/Home';
import { Link } from 'react-router-dom';
import './App.css';
import Edit from './components/CURD/Edit/Edit';

function App() {

  const API_STATES = {
    LOADED: "loaded",
    LOADING: 'loading',
    ERROR: 'error'
  }

  const [error, setError] = useState("");
  const [apiStatus, setApiStatus] = useState(API_STATES.LOADING);
  const [products, setProducts] = useState([]);

  useEffect(() => {
    axios
      .get("https://fakestoreapi.com/products")
      .then((response) => {
        setProducts(response.data);
        setApiStatus(API_STATES.LOADED);
      })
      .catch((error) => {
        console.error(error);
        setError("Failed to fetch products. Please try again later.");
        setApiStatus(API_STATES.ERROR);
      });
  }, [API_STATES.ERROR,API_STATES.LOADED]);

  const deleteProduct = (productId) => {
    const filteredProducts = products.filter((product) => {
      return product.id !== productId;
    });
    return setProducts(filteredProducts);
  }

  const addProduct = (newProduct) => {
    let appendProduct = [newProduct, ...products];
    return setProducts(appendProduct);
  }

  const editProduct = (newProduct) => {
    const updatedProducts = products.map((product) => {
      if (product.id === parseInt(newProduct.id)) {
        return {
          ...product,
          title: newProduct.title,
          description: newProduct.description,
          category: newProduct.category,
          image: newProduct.image,
          price: newProduct.price,
          rating: {
            ...product.rating,
            rate: newProduct.rating.rate,
            count: newProduct.rating.count
          }
        }
      } else {
        return product;
      }
    });

    setProducts(updatedProducts);
  }

  const [isAddProductClicked, setIsAddProductClicked] = useState(false);

  function headerHomeClick(state) {
    if (state === true) {
      setIsAddProductClicked(false)
    }
  }

  return (
    <div className='App'>
      <Header onHomeClick={headerHomeClick} />


      {
        apiStatus === API_STATES.LOADING &&
        <div className='loaderDisplay'>
          <Loader />
        </div>
      }
      {
        apiStatus === API_STATES.ERROR &&
        <div className='errorDisplay'>
          {error}
        </div>
      }

      {
        apiStatus === API_STATES.LOADED &&
        products.length === 0 &&
        <div className='emptyErrorDisplay'>
          No Products Avaiable
        </div>
      }

      {apiStatus === API_STATES.LOADED &&
        products.length > 0 &&


        <div className='productDisplay'>

          {!isAddProductClicked &&
            <Link to='/add' className='header-btn add-product-button' onClick={() => setIsAddProductClicked(true)}>
              Add Product
            </Link>
          }

          <Routes>
            <Route path="/" element={<Home apiData={products} deleteProduct={deleteProduct} />} />
            <Route path="/add" element={<Add addProduct={addProduct} apiData={products} />} />
            <Route path="/product/:productId" element={<ProductPage />} />
            <Route path="/edit/:productId" element={<Edit editProduct={editProduct} apiData={products} />} />
          </Routes>

        </div>

      }

    </div>
  )
}

export default App;
