import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import Loader from '../Loader/Loader';

import './ProductPage.css'


function ProductPage(props) {
    const { productId } = useParams();


    const PRODUCT_STATES = {
        LOADING: "loading",
        LOADED: "loaded",
        ERROR: "error",
    }

    const [error, setError] = useState("");
    const [apiProduct, setApiProduct] = useState(PRODUCT_STATES.LOADING);
    const [product, setProduct] = useState([]);

    useEffect(() => {
        fetch(`https://fakestoreapi.com/products/${productId}`)
            .then((response) => {
                return response.json();
            })
            .then((productData) => {
                setApiProduct(PRODUCT_STATES.LOADED);
                setProduct(productData);
            })
            .catch((errorWhileFetching) => {
                console.error(errorWhileFetching);
                setApiProduct(PRODUCT_STATES.ERROR);
                setError("Unable to fetch data from server, Please try again after sometime");
            })
    }, [productId,PRODUCT_STATES.LOADING,PRODUCT_STATES.LOADED,PRODUCT_STATES.ERROR]);


    return (
        <div className="product-page">

            {
                apiProduct === PRODUCT_STATES.LOADING &&
                <div className='loaderDisplay'>
                    <Loader />
                </div>
            }

            {
                apiProduct === PRODUCT_STATES.ERROR &&
                <div className='errorDisplay'>
                    {error}
                </div>
            }

            {
                apiProduct === PRODUCT_STATES.LOADED &&
                !product &&
                <div className='emptyErrorDisplay'>
                    No Products recieved from server. Please try later.
                </div>
            }


            {
                apiProduct === PRODUCT_STATES.LOADED &&
                (product) &&
                <div>
                    <div key={product.id} className="productPage-card">

                        <img
                            className='productImage'
                            key={product.id} src={product.image}
                            width="500"
                            height="600"
                            alt={product.title}
                        />

                        <div className='info-section'>

                            <div className="title">
                                {product.title}
                            </div>
                            <div className="category">
                                Category : {product.category}
                            </div>

                            <div className="price">
                                ${product.price}
                            </div>

                            <div className="description">
                                {product.description}
                            </div>

                            <div className="rating">
                                <div>
                                    <i className="fa-solid fa-star rate-star"></i>
                                    {product.rating.rate} / 5
                                </div>
                                <div>
                                    <i className='fas fa-users'></i>
                                    {product.rating.count}
                                </div>
                            </div>

                            <div className="cart-button product-button">
                                Add to Cart <i className="fa-solid fa-cart-plus cart-icon"></i>
                            </div>

                        </div>

                    </div>
                </div>
            }
        </div>
    )

}

export default ProductPage;
