import React, { useState, useEffect } from 'react';
import LoadProducts from '../LoadProducts/LoadProducts';

function Home(props) {
 

  function deleteProductHome(id) {
    return props.deleteProduct(id);
  }

  const apiData = props.apiData;


  return (
    <div>
        <div>
          <LoadProducts apiData={apiData} deleteProductHome={deleteProductHome} />
        </div>
    </div>
  )
}

export default Home;