

function Parent() {

    const [products, setProducts] = useState([]);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);

    useEffect(() => {
        axios
            .get("https://fakestoreapi.com/products")
            .then((response) => {
                setProducts(response.data);
                console.log(response.data);
                setLoading(false);
            })
            .catch((error) => {
                console.error(error);
                setError("Failed to fetch products. Please try again later.");
                setLoading(false);
            });
    }, []);

    const deleteProduct = (productId) => {
        const filteredProducts = products.filter((product) => {
            return product.id !== productId;
        });
        return setProducts(filteredProducts);
    }

  
    
    return (
        <>
            <Header />
            <div className='accountOptions'>
                <Link to='/add' className='header-btn add-button'>
                    Add Product
                </Link>
            </div>
            {loading &&
                <div className='loaderDisplay'>
                  <Loader/>
                </div>
            }

            {error &&
                <div className='errorDisplay'>
                 {error}
               </div>
            }

            {!loading && !error &&
                <Home apiData={products} deleteProduct={deleteProduct} />
            }
        </>
    );
}

// export default Parent;
