import { useState } from 'react';
import { v4 as uuidv4 } from "uuid";
import { Link } from 'react-router-dom';
import './Add.css';

function Add(props) {

    const [error, setError] = useState({
        categoryError: "",
        descriptionError: "",
        priceError: "",
        rateError: "",
        countError: "",
        titleError: "",
        imageError: ""
    });

    const [formData, setFormData] = useState({
        category: "",
        description: "",
        id: uuidv4(),
        price: "",
        rating: {
            rate: 0,
            count: 0,
        },
        title: "",
        image: ""
    });

    const handleChange = (event) => {
        let nameUpdate = event.target.name;
        let valueUpdate = event.target.value;

        if (valueUpdate.length === 0) {
            setError({ ...error, [`${nameUpdate}Error`]: `${nameUpdate} cannot be empty` });
        }
        else {
            setError({ ...error, [`${nameUpdate}Error`]: `` });
        }
        formData[nameUpdate] = valueUpdate;
    }

    const handleRatingChange = (event) => {
        let nameUpdate = event.target.name;
        let valueUpdate = event.target.value;


        if (valueUpdate.length === 0) {
            console.log(valueUpdate, "sadsa");
            setError({ ...error, [`${nameUpdate}Error`]: `${nameUpdate} cannot be empty` });
        }
        else if (nameUpdate == 'rate') {
            if (isNaN(valueUpdate) || parseFloat(valueUpdate) > 5) {
                setError({ ...error, [`${nameUpdate}Error`]: `${nameUpdate} must be an Integer or a Decimal value between 0-5` });
            }
            else {
                setError({ ...error, [`${nameUpdate}Error`]: `` });
            }
        }
        else if (nameUpdate == 'count') {
            if (isNaN(valueUpdate) || valueUpdate.split('').includes(".")) {
                setError({ ...error, [`${nameUpdate}Error`]: `${nameUpdate} must be an Integer` });
            }
            else {
                setError({ ...error, [`${nameUpdate}Error`]: `` });
            }
        }
        else {
            setError({ ...error, [`${nameUpdate}Error`]: `` });
        }
        formData.rating[event.target.name] = event.target.value;
    }


    const [productExistMessage, setProductExistMessage] = useState("");
    const [productEmptyMessage, setProductEmptyMessage] = useState("");
    const [productAddedMessage, setProductAddedMessage] = useState("");

    const handleSubmit = (event) => {
        event.preventDefault();

        const { title, category, description, price, rating: { rate, count } } = formData;

        const isProductAdded = props.apiData.some(product => (
            product.title === title &&
            product.category === category &&
            product.description === description &&
            product.price === price &&
            product.rating.rate === rate &&
            product.rating.count === count
        ));

        const emptyField = () => {
            const emptyItems = Object.values(formData).filter((inputValue) => {
                return (inputValue.length === 0);
            })
            console.log(emptyItems);
            return (emptyItems.length > 0);
        }

        if (isProductAdded) {
            console.log(props.apiData);
            setProductExistMessage("Given item is already added in Cart.");
            setProductEmptyMessage("");
            setProductAddedMessage("");
        }
        else if (emptyField()) {
            setProductEmptyMessage("All input for product must be entered.");
            setProductExistMessage("");
            setProductAddedMessage("");
        }
        else {
            setProductEmptyMessage("");
            setProductExistMessage("");
            setProductAddedMessage(`${formData.title} added in product List.`);
            setFormData({
                category: "",
                description: "",
                id: uuidv4(),
                price: "",
                rating: {
                    rate: "",
                    count: "",
                },
                title: "",
                image: ""
            })
            props.addProduct(formData);
        }
    }
    return (
        <div className="add-page">

            <form className="add-details" onSubmit={handleSubmit}>
                <div className="add-heading">
                    Add a Product
                </div>

                {
                    productExistMessage &&
                    <h2 className='productExistMessage'>{productExistMessage}</h2>
                }
                {
                    productEmptyMessage &&
                    <h2 className='productEmptyMessage'>{productEmptyMessage}</h2>
                }
                {
                    productAddedMessage &&
                    <h2 className='productAddMessage'>{productAddedMessage}</h2>
                }


                <div className="add-data">
                    <div className="add-title">
                        Product category
                    </div>
                    <input type="text" name="category" onChange={handleChange}   value={formData.category} className="add-input" />
                </div>
                {error.categoryError && <div className='error'>{error.categoryError}</div>}

                <div className="add-data add-description">
                    <div className="add-title">
                        Product description
                    </div>
                    <input type="text" name="description" onChange={handleChange}  value={formData.description} className="add-input" />
                </div>
                {error.descriptionError && <div  className='error'>{error.descriptionError}</div>}

                <div className="add-data">
                    <div className="add-title">
                        Product image
                    </div>
                    <input type="text" name="image" onChange={handleChange}  value={formData.image} className="add-input" />
                </div>
                {error.imageError && <div  className='error'>{error.imageError}</div>}

                <div className="add-data">
                    <div className="add-title">
                        Product price
                    </div>
                    <input type="text" name="price" onChange={handleChange}  value={formData.price} className="add-input" />
                </div>
                {error.priceError && <div  className='error'>{error.priceError}</div>}


                <div className="add-data">
                    <div className="add-title">
                        Product title
                    </div>
                    <input type="text" name="title" onChange={handleChange} value={formData.title} className="add-input" />
                </div>
                {error.titleError && <div  className='error'>{error.titleError}</div>}

                <div className="add-data">
                    <div className="add-title">
                        Product Rating
                    </div>
                    <input type="text" name="rate" onChange={handleRatingChange} value={formData.rating.rate} className="add-input" />
                </div>
                {error.rateError && <div  className='error'>{error.rateError}</div>}

                <div className="add-data">
                    <div className="add-title">
                        Product User Count
                    </div>
                    <input type="text" name="count" onChange={handleRatingChange} value={formData.rating.count} className="add-input" />
                </div>
                {error.countError && <div  className='error'>{error.countError}</div>}


                <button type="submit" className="add-submit-button" >
                    Submit
                </button>

            </form>
        </div>
    )
}


export default Add;