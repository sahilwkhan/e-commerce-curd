import { useState } from 'react';
import { useParams } from 'react-router-dom';
import './Edit.css';



function Edit(props) {

    const originalProductID = useParams().productId
    const originalProductList = props.apiData.filter((product) => {
        return (product.id === parseInt(originalProductID));
    })
    const originalProduct = originalProductList[0];



    const [error, setError] = useState({
        categoryError: "",
        descriptionError: "",
        priceError: "",
        rateError: "",
        countError: "",
        titleError: "",
        imageError: "",
    });

    const [editformData, setEditformData] = useState({
        category: originalProduct.category,
        description: originalProduct.description,
        id: originalProductID,
        image: originalProduct.image,
        price: originalProduct.price,
        rating: {
            rate: originalProduct.rating.rate,
            count: originalProduct.rating.count,
        },
        title: originalProduct.title
    });

    const [isProductAdded, setIsProductAdded] = useState(false);

    const handleChange = (event) => {
        let nameUpdate = event.target.name;
        let valueUpdate = event.target.value;

        if (valueUpdate.length === 0) {
            setError({ ...error, [`${nameUpdate}Error`]: `${nameUpdate} cannot be empty` });
        }
        else {
            setError({ ...error, [`${nameUpdate}Error`]: `` });
        }
        editformData[nameUpdate] = valueUpdate;
        props.editProduct(editformData);
    }

    const handleEditRatingChange = (event) => {
        let nameUpdate = event.target.name;
        let valueUpdate = event.target.value;


        if (valueUpdate.length === 0) {
            setError({ ...error, [`${nameUpdate}Error`]: `${nameUpdate} cannot be empty` });
        }
        else if (nameUpdate === 'rate') {
            if (isNaN(valueUpdate) || parseFloat(valueUpdate) > 5) {
                setError({ ...error, [`${nameUpdate}Error`]: `${nameUpdate} must be an Integer or a Decimal value between 0-5` });
            }
            else {
                setError({ ...error, [`${nameUpdate}Error`]: `` });
            }
        }
        else if (nameUpdate === 'count') {
            if (isNaN(valueUpdate) || valueUpdate.split('').includes(".")) {
                setError({ ...error, [`${nameUpdate}Error`]: `${nameUpdate} must be an Integer` });
            }
            else {
                setError({ ...error, [`${nameUpdate}Error`]: `` });
            }
        }
        else {
            setError({ ...error, [`${nameUpdate}Error`]: `` });
        }
        editformData.rating[event.target.name] = event.target.value;
        props.editProduct(editformData);
    }

    const handleEditSubmit = (event) => {
        event.preventDefault();

        console.log(editformData, "Product Added.");
        props.editProduct(editformData);
        
    }
    return (
        <div className="edit-page">

            <form className="edit-details" onSubmit={handleEditSubmit}>
                <div className='original-product'>
                    <div key={originalProduct.id} className="product-card">

                        <div to={`/product/${originalProduct.id}`} >
                            <img
                                className='productImages'
                                key={originalProduct.id} src={originalProduct.image}
                                alt={originalProduct.title}
                            />
                        </div>

                        <div className="title">
                            {originalProduct.title}
                        </div>

                        <div className="category">
                            Category : {originalProduct.category}
                        </div>

                        <div className="price">
                            ${originalProduct.price}
                        </div>

                        <div className="rating">
                            <div>
                                <i className="fa-solid fa-star rate-star"></i>
                                {originalProduct.rating.rate} / 5
                            </div>
                            <div>
                                <i className='fas fa-users'></i>
                                {originalProduct.rating.count}
                            </div>
                        </div>
                    </div>
                </div>

                <div className="edit-heading">
                    Edit Product
                </div>

                <div className="edit-data">
                    <div className="edit-title">
                        Product category
                    </div>
                    <input
                        type="text"
                        name="category"
                        onChange={handleChange}
                        value={editformData.category}
                        className="add-input"
                    />
                </div>
                {error.categoryError && <div className='error'>{error.categoryError}</div>}

                <div className="edit-data add-description">
                    <div className="edit-title">
                        Product description
                    </div>
                    <input type="text"
                        name="description"
                        onChange={handleChange}
                        value={editformData.description}
                        className="add-input"
                    />
                </div>
                {error.descriptionError && <div className='error'>{error.descriptionError}</div>}


                <div className="edit-data">
                    <div className="edit-title">
                        Product image
                    </div>
                    <input
                        type="text"
                        name="image"
                        onChange={handleChange}
                        value={editformData.image}
                        className="add-input"
                    />
                </div>
                {error.imageError && <div className='error'>{error.imageError}</div>}



                <div className="edit-data">
                    <div className="edit-title">
                        Product price
                    </div>
                    <input
                        type="text"
                        name="price"
                        onChange={handleChange}
                        value={editformData.price}
                        className="edit-input"
                    />
                </div>
                {error.priceError && <div className='error'>{error.priceError}</div>}


                <div className="edit-data">
                    <div className="edit-title">
                        Product title
                    </div>
                    <input
                        type="text"
                        name="title"
                        onChange={handleChange}
                        value={editformData.title}
                        className="add-input"
                    />
                </div>
                {error.titleError && <div className='error'>{error.titleError}</div>}


                <div className="edit-data">
                    <div className="edit-title">
                        Product Rating
                    </div>
                    <input
                        type="text"
                        name="rate"
                        onChange={handleEditRatingChange}
                        value={editformData.rating.rate}
                        className="add-input"
                    />
                </div>
                {error.rateError && <div className='error'>{error.rateError}</div>}


                <div className="edit-data">
                    <div className="edit-title">
                        Product User Count
                    </div>
                    <input
                        type="text"
                        name="count"
                        onChange={handleEditRatingChange}
                        value={editformData.rating.count}
                        className="add-input"
                    />
                </div>
                {error.countError && <div className='error'>{error.countError}</div>}



                <button type="submit" className="edit-submit-button" >
                    Submit
                </button>

            </form>
        </div>
    )
}


export default Edit;