import React from 'react';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import './Header.css';

function Header(props) {


    const homeClick = ()=>{
        props.onHomeClick(true);
    }
    
    const [cartItemCount, setCartItemCount] = useState(0);

    const cartItems = JSON.parse(localStorage.getItem('cart'));
    useEffect(() => {
        if (cartItems) {
            setCartItemCount(cartItems.length);
        }
        else {
            setCartItemCount(0);
        }
    },[cartItems]);

    return (
        <header className="headerStyle">
            <div className='logo'>
                <img
                    className='logoStyle'
                    src='https://cdn.dribbble.com/users/3267379/screenshots/6098927/e_shop.jpg'
                    alt="store-logo"
                />
                <div>Fake Store</div>
            </div>
            <div className='accountOptions'>
                <Link to='/' className='header-btn add-button' onClick={homeClick}>
                    Home
                </Link>
            </div>
        </header>

    );
}

export default Header;