import { Link } from 'react-router-dom';

import './LoadProducts.css';

function LoadProducts(props) {


    function handleDelete(id) {
        return props.deleteProductHome(id);
    }
    
    const { apiData } = props;
    const idList = [];
    const filteredProducts = apiData.filter((product) => {
        if (idList.includes(product.id)) {
            console.log(product,"Product already exist in the cart.");
            return false;
        }
        else {
            idList.push(product.id);
            return true;
        }
    })
    
    return (
        <div className="product-container">
            {
                filteredProducts.map((productArray) => {
                    return (
                        <div key={productArray.id} className="product-card">

                            <Link to={`/product/${productArray.id}`} >
                                <img
                                    className='productImages'
                                    key={productArray.id} src={productArray.image}
                                    alt={productArray.title}
                                />
                            </Link>

                            <div className="title">
                                <div>
                                {productArray.title}                           
                                </div>
                            </div>

                            <div className="category">
                                Category : {productArray.category}
                            </div>

                            <div className="price">
                                ${productArray.price}
                            </div>

                            <div className="rating">
                                <div>
                                    <i className="fa-solid fa-star rate-star"></i>
                                    {productArray.rating.rate} / 5
                                </div>
                                <div>
                                    <i className='fas fa-users'></i>
                                    {productArray.rating.count}
                                </div>
                            </div>

                            <div className="cart-button" >
                                Add to cart <i className="fa-solid fa-cart-plus cart-icon"></i>
                            </div>

                            <div className="curd-button">
                                <Link
                                    to={`/edit/${productArray.id}`}
                                    className="edit curd-hover"
                                >
                                    Edit
                                </Link>
                                <div
                                    to={`/delete/${productArray.id}`}
                                    className='delete curd-hover'
                                    onClick={
                                        () => handleDelete(productArray.id)
                                    }
                                >
                                    Delete
                                </div>
                            </div>
                        </div>
                    )
                })
            }
        </div>
    );
}

export default LoadProducts;